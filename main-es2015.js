(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<router-outlet></router-outlet>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main/main.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main/main.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"slide slide--first\">\n    <div class=\"container text-center\">\n        <img class=\"logo\" src=\"assets/img/logo_qr.png\">\n        <div style=\"color: black; background-color: rgba(255, 255, 255, 0.596); border-radius: 10px; padding: 40px 20px;\" class=\"mt-5\">\n            Corsica is the Island of Beauty with red cliffs plunging in the cristal blue waters. There is over 1,000\n            kilometers of coastline, which includes the iconic Scandola reserve, popular for scuba diving. Corsica has\n            hidden coves, sandy beaches and craggy peninsulas and was the homeland of Napoleon. This is land of\n            tradition and history, best discovered on walks through its cities.\n            <br><br>\n            Corsica wants to see you soon as possible to enjoy summer holiday.\n        </div>\n        <!-- <div class=\"mt-3\"> -->\n        <!-- <h1>Corsica Passport</h1>\n            <h5>Docteur Paul Marcaggi<br>& Corcode team</h5> -->\n        <!-- </div> -->\n    </div>\n    <div class=\"footer__menu\">\n        <a routerLink=\"/assets/legalMentions.pdf\" target=\"_blank\">Legal mentions</a>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/test/test.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/test/test.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"slide slide--first\">\n    <div class=\"container text-center\">\n        <img class=\"logo\" src=\"assets/img/logo_qr.png\">\n        <div style=\"color: black; background-color: rgba(255, 255, 255, 0.596); border-radius: 10px; padding: 40px 20px;\" class=\"mt-5\">\n            Corsica is the Island of Beauty with red cliffs plunging in the cristal blue waters. There is over 1,000\n            kilometers of coastline, which includes the iconic Scandola reserve, popular for scuba diving. Corsica has\n            hidden coves, sandy beaches and craggy peninsulas and was the homeland of Napoleon. This is land of\n            tradition and history, best discovered on walks through its cities.\n            <br><br>\n            Corsica wants to see you soon as possible to enjoy summer holiday.\n        </div>\n        <!-- <div class=\"mt-3\"> -->\n        <!-- <h1>Corsica Passport</h1>\n            <h5>Docteur Paul Marcaggi<br>& Corcode team</h5> -->\n        <!-- </div> -->\n    </div>\n    <div class=\"d-flex justify-content-center\">\n        <a href=\"https://transactions.sendowl.com/products/78282910/F3E80A07/purchase\" rel=\"nofollow\"><img src=\"https://transactions.sendowl.com/assets/external/buy-now.png\" /></a><script type=\"text/javascript\" src=\"https://transactions.sendowl.com/assets/sendowl.js\" ></script>\n    </div>\n    <div class=\"footer__menu\">\n        <a routerLink=\"/assets/legalMentions.pdf\" target=\"_blank\">Legal mentions</a>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __createBinding, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault, __classPrivateFieldGet, __classPrivateFieldSet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__createBinding", function() { return __createBinding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldGet", function() { return __classPrivateFieldGet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldSet", function() { return __classPrivateFieldSet; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __createBinding(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}

function __exportStar(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}

function __classPrivateFieldGet(receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
}

function __classPrivateFieldSet(receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _pages_main_main_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages/main/main.component */ "./src/app/pages/main/main.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _pages_test_test_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/test/test.component */ "./src/app/pages/test/test.component.ts");





const routes = [
    { component: _pages_main_main_component__WEBPACK_IMPORTED_MODULE_1__["MainComponent"], path: '' },
    { component: _pages_test_test_component__WEBPACK_IMPORTED_MODULE_4__["TestComponent"], path: 'test' }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvYXBwLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'corsicaPassportWeb';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _pages_main_main_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/main/main.component */ "./src/app/pages/main/main.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _pages_test_test_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pages/test/test.component */ "./src/app/pages/test/test.component.ts");








let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _pages_main_main_component__WEBPACK_IMPORTED_MODULE_5__["MainComponent"],
            _pages_test_test_component__WEBPACK_IMPORTED_MODULE_7__["TestComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/pages/main/main.component.scss":
/*!************************************************!*\
  !*** ./src/app/pages/main/main.component.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n.primary-block {\n  background-color: #132d74;\n  color: white;\n}\n.logo {\n  border-radius: 10px;\n  max-width: 400px;\n}\n@media (max-height: 400px) {\n  .content-align {\n    padding-top: 20px;\n  }\n}\nh1, h2, h3, h4, h5 {\n  text-align: center;\n}\nul {\n  list-style: none;\n  /* Remove default bullets */\n}\nul li::before {\n  content: \"•\";\n  /* Add content: \\2022 is the CSS Code/unicode for a bullet */\n  color: #132d74;\n  /* Change the color */\n  font-weight: bold;\n  /* If you want it to be bold */\n  display: inline-block;\n  /* Needed to add space between the bullet and the text */\n  width: 1em;\n  /* Also needed for space (tweak if needed) */\n  margin-left: -1em;\n  /* Also needed for space (tweak if needed) */\n}\nb {\n  color: #132d74;\n}\n.box {\n  border: 2px solid #132d74;\n  margin: 10px;\n  padding: 10px 20px;\n  min-height: 120px;\n  display: flex;\n  align-items: center;\n}\n.slide {\n  min-height: 700px;\n  padding-top: 70px;\n  padding-bottom: 70px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n.slide.slide--first {\n  background-image: url(\"/assets/img/background2.jpg\");\n  background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover;\n  height: 100vh;\n  justify-content: space-between;\n}\n.slide:nth-child(odd) {\n  background-color: #132d74;\n  color: white;\n}\n.slide:nth-child(odd) b,\n.slide:nth-child(odd) ul li::before {\n  color: white;\n}\n.slide:nth-child(odd) .box {\n  border: 2px solid white;\n}\n.footer a, .slide--first a {\n  color: white;\n  text-decoration: underline;\n}\n.footer .footer__menu, .slide--first .footer__menu {\n  position: absolute;\n  bottom: 10px;\n  left: 0px;\n  margin: 20px 0;\n  text-align: center;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy9tYWluL21haW4uY29tcG9uZW50LnNjc3MiLCIvVXNlcnMvY29yY29kZS93b3JrcGxhY2UvY29yc2lrL2NvcnNpY2EtcGFzc3BvcnQtd2ViL3NyYy9hcHAvcGFnZXMvbWFpbi9tYWluLmNvbXBvbmVudC5zY3NzIiwiL1VzZXJzL2NvcmNvZGUvd29ya3BsYWNlL2NvcnNpay9jb3JzaWNhLXBhc3Nwb3J0LXdlYi9zcmMvc2Nzcy12YXJpYWJsZXMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUNFaEI7RUFDSSx5QkNITTtFRElOLFlDRG9CO0FGQ3hCO0FDR0E7RUFDSSxtQkFBQTtFQUNBLGdCQUFBO0FEQUo7QUNJQTtFQUNJO0lBQ0ksaUJBQUE7RURETjtBQUNGO0FDSUE7RUFDSSxrQkFBQTtBREZKO0FDS0E7RUFDSSxnQkFBQTtFQUFrQiwyQkFBQTtBRER0QjtBQ0lBO0VBQ0ksWUFBQTtFQUFtQiw0REFBQTtFQUNuQixjQzdCTTtFRDZCVyxxQkFBQTtFQUNqQixpQkFBQTtFQUFtQiw4QkFBQTtFQUNuQixxQkFBQTtFQUF1Qix3REFBQTtFQUN2QixVQUFBO0VBQVksNENBQUE7RUFDWixpQkFBQTtFQUFtQiw0Q0FBQTtBREt2QjtBQ0ZBO0VBQ0ksY0NyQ007QUYwQ1Y7QUNGQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QURLSjtBQ0ZBO0VBQ0ksaUJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7QURLSjtBQ0hJO0VBQ0Msb0RBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSxhQUFBO0VBQ0EsOEJBQUE7QURLTDtBQ0ZJO0VBQ0kseUJDbkVFO0VEb0VGLFlDakVnQjtBRnFFeEI7QUNIUTs7RUFFSSxZQ3BFWTtBRnlFeEI7QUNGUTtFQUNJLHVCQUFBO0FESVo7QUNLSTtFQUNJLFlBQUE7RUFDQSwwQkFBQTtBREZSO0FDS0k7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0VBRUEsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBREpSIiwiZmlsZSI6ImFwcC9wYWdlcy9tYWluL21haW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAY2hhcnNldCBcIlVURi04XCI7XG4ucHJpbWFyeS1ibG9jayB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxMzJkNzQ7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmxvZ28ge1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBtYXgtd2lkdGg6IDQwMHB4O1xufVxuXG5AbWVkaWEgKG1heC1oZWlnaHQ6IDQwMHB4KSB7XG4gIC5jb250ZW50LWFsaWduIHtcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcbiAgfVxufVxuaDEsIGgyLCBoMywgaDQsIGg1IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG51bCB7XG4gIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIC8qIFJlbW92ZSBkZWZhdWx0IGJ1bGxldHMgKi9cbn1cblxudWwgbGk6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwi4oCiXCI7XG4gIC8qIEFkZCBjb250ZW50OiBcXDIwMjIgaXMgdGhlIENTUyBDb2RlL3VuaWNvZGUgZm9yIGEgYnVsbGV0ICovXG4gIGNvbG9yOiAjMTMyZDc0O1xuICAvKiBDaGFuZ2UgdGhlIGNvbG9yICovXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAvKiBJZiB5b3Ugd2FudCBpdCB0byBiZSBib2xkICovXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgLyogTmVlZGVkIHRvIGFkZCBzcGFjZSBiZXR3ZWVuIHRoZSBidWxsZXQgYW5kIHRoZSB0ZXh0ICovXG4gIHdpZHRoOiAxZW07XG4gIC8qIEFsc28gbmVlZGVkIGZvciBzcGFjZSAodHdlYWsgaWYgbmVlZGVkKSAqL1xuICBtYXJnaW4tbGVmdDogLTFlbTtcbiAgLyogQWxzbyBuZWVkZWQgZm9yIHNwYWNlICh0d2VhayBpZiBuZWVkZWQpICovXG59XG5cbmIge1xuICBjb2xvcjogIzEzMmQ3NDtcbn1cblxuLmJveCB7XG4gIGJvcmRlcjogMnB4IHNvbGlkICMxMzJkNzQ7XG4gIG1hcmdpbjogMTBweDtcbiAgcGFkZGluZzogMTBweCAyMHB4O1xuICBtaW4taGVpZ2h0OiAxMjBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLnNsaWRlIHtcbiAgbWluLWhlaWdodDogNzAwcHg7XG4gIHBhZGRpbmctdG9wOiA3MHB4O1xuICBwYWRkaW5nLWJvdHRvbTogNzBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4uc2xpZGUuc2xpZGUtLWZpcnN0IHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWcvYmFja2dyb3VuZDIuanBnXCIpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGhlaWdodDogMTAwdmg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5zbGlkZTpudGgtY2hpbGQob2RkKSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxMzJkNzQ7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5zbGlkZTpudGgtY2hpbGQob2RkKSBiLFxuLnNsaWRlOm50aC1jaGlsZChvZGQpIHVsIGxpOjpiZWZvcmUge1xuICBjb2xvcjogd2hpdGU7XG59XG4uc2xpZGU6bnRoLWNoaWxkKG9kZCkgLmJveCB7XG4gIGJvcmRlcjogMnB4IHNvbGlkIHdoaXRlO1xufVxuXG4uZm9vdGVyIGEsIC5zbGlkZS0tZmlyc3QgYSB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59XG4uZm9vdGVyIC5mb290ZXJfX21lbnUsIC5zbGlkZS0tZmlyc3QgLmZvb3Rlcl9fbWVudSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAxMHB4O1xuICBsZWZ0OiAwcHg7XG4gIG1hcmdpbjogMjBweCAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xufSIsIkBpbXBvcnQgJy4vLi4vLi4vLi4vc2Nzcy12YXJpYWJsZXMuc2Nzcyc7XG5cbi5wcmltYXJ5LWJsb2NrIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeTtcbiAgICBjb2xvcjogJGZvbnQtY29sb3Itb24tcHJpbWFyeTtcbn1cblxuLmxvZ28ge1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgbWF4LXdpZHRoOiA0MDBweDtcbiAgICAvLyBtYXJnaW4tYm90dG9tOiAxMDBweDtcbn1cblxuQG1lZGlhIChtYXgtaGVpZ2h0OiA0MDBweCkge1xuICAgIC5jb250ZW50LWFsaWduIHtcbiAgICAgICAgcGFkZGluZy10b3A6IDIwcHg7XG4gICAgfVxufVxuXG5oMSwgaDIsIGgzLCBoNCwgaDUge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxudWwge1xuICAgIGxpc3Qtc3R5bGU6IG5vbmU7IC8qIFJlbW92ZSBkZWZhdWx0IGJ1bGxldHMgKi9cbiAgfVxuICBcbnVsIGxpOjpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXFwyMDIyXCI7ICAvKiBBZGQgY29udGVudDogXFwyMDIyIGlzIHRoZSBDU1MgQ29kZS91bmljb2RlIGZvciBhIGJ1bGxldCAqL1xuICAgIGNvbG9yOiAkcHJpbWFyeTsgLyogQ2hhbmdlIHRoZSBjb2xvciAqL1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkOyAvKiBJZiB5b3Ugd2FudCBpdCB0byBiZSBib2xkICovXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrOyAvKiBOZWVkZWQgdG8gYWRkIHNwYWNlIGJldHdlZW4gdGhlIGJ1bGxldCBhbmQgdGhlIHRleHQgKi9cbiAgICB3aWR0aDogMWVtOyAvKiBBbHNvIG5lZWRlZCBmb3Igc3BhY2UgKHR3ZWFrIGlmIG5lZWRlZCkgKi9cbiAgICBtYXJnaW4tbGVmdDogLTFlbTsgLyogQWxzbyBuZWVkZWQgZm9yIHNwYWNlICh0d2VhayBpZiBuZWVkZWQpICovXG59XG5cbmIge1xuICAgIGNvbG9yOiAkcHJpbWFyeTtcbn1cblxuLmJveCB7XG4gICAgYm9yZGVyOiAycHggc29saWQgJHByaW1hcnk7XG4gICAgbWFyZ2luOiAxMHB4O1xuICAgIHBhZGRpbmc6IDEwcHggMjBweDtcbiAgICBtaW4taGVpZ2h0OiAxMjBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5zbGlkZSB7XG4gICAgbWluLWhlaWdodDogNzAwcHg7XG4gICAgcGFkZGluZy10b3A6IDcwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDcwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgJi5zbGlkZS0tZmlyc3Qge1xuICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9hc3NldHMvaW1nL2JhY2tncm91bmQyLmpwZycpO1xuICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgIGhlaWdodDogMTAwdmg7XG4gICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICB9XG5cbiAgICAmOm50aC1jaGlsZChvZGQpIHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnk7XG4gICAgICAgIGNvbG9yOiAkZm9udC1jb2xvci1vbi1wcmltYXJ5O1xuICAgICAgICBiLFxuICAgICAgICB1bCBsaTo6YmVmb3JlIHtcbiAgICAgICAgICAgIGNvbG9yOiAkZm9udC1jb2xvci1vbi1wcmltYXJ5XG4gICAgICAgIH1cblxuICAgICAgICAuYm94IHtcbiAgICAgICAgICAgIGJvcmRlcjogMnB4IHNvbGlkIHdoaXRlO1xuICAgICAgICB9XG4gICAgfVxufVxuXG4uZm9vdGVyLCAuc2xpZGUtLWZpcnN0IHtcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjksIDI5LCAyOSk7XG4gICAgLy8gY29sb3I6IHdoaXRlO1xuICAgIC8vIHBhZGRpbmc6IDIwcHg7XG4gICAgYSB7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG4gICAgfVxuXG4gICAgLmZvb3Rlcl9fbWVudSB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgYm90dG9tOiAxMHB4O1xuICAgICAgICBsZWZ0OiAwcHg7XG4gICAgICAgIC8vIGJvcmRlci10b3A6IDFweCBzb2xpZCB3aGl0ZTtcbiAgICAgICAgbWFyZ2luOiAyMHB4IDA7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuICAgIC8vIHVsIGxpOjpiZWZvcmUge1xuICAgIC8vICAgICBjb2xvcjogd2hpdGU7XG4gICAgLy8gfVxufSIsIiRwcmltYXJ5OiAjMTMyZDc0OyAvLyAjNGM5ZmQ2O1xuJHByaW1hcnkyOiAjZTJmM2ZmO1xuJHNlY29uZGFyeTogcmdiKDEyMywgMTM5LCAyMDkpO1xuJGZvbnQtY29sb3Itb24tcHJpbWFyeTogd2hpdGU7Il19 */");

/***/ }),

/***/ "./src/app/pages/main/main.component.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/main/main.component.ts ***!
  \**********************************************/
/*! exports provided: MainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainComponent", function() { return MainComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let MainComponent = class MainComponent {
    constructor() { }
    ngOnInit() {
    }
};
MainComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-main',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./main.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/main/main.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./main.component.scss */ "./src/app/pages/main/main.component.scss")).default]
    })
], MainComponent);



/***/ }),

/***/ "./src/app/pages/test/test.component.scss":
/*!************************************************!*\
  !*** ./src/app/pages/test/test.component.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n.primary-block {\n  background-color: #132d74;\n  color: white;\n}\n.logo {\n  border-radius: 10px;\n  max-width: 400px;\n}\n@media (max-height: 400px) {\n  .content-align {\n    padding-top: 20px;\n  }\n}\nh1, h2, h3, h4, h5 {\n  text-align: center;\n}\nul {\n  list-style: none;\n  /* Remove default bullets */\n}\nul li::before {\n  content: \"•\";\n  /* Add content: \\2022 is the CSS Code/unicode for a bullet */\n  color: #132d74;\n  /* Change the color */\n  font-weight: bold;\n  /* If you want it to be bold */\n  display: inline-block;\n  /* Needed to add space between the bullet and the text */\n  width: 1em;\n  /* Also needed for space (tweak if needed) */\n  margin-left: -1em;\n  /* Also needed for space (tweak if needed) */\n}\nb {\n  color: #132d74;\n}\n.box {\n  border: 2px solid #132d74;\n  margin: 10px;\n  padding: 10px 20px;\n  min-height: 120px;\n  display: flex;\n  align-items: center;\n}\n.slide {\n  min-height: 700px;\n  padding-top: 70px;\n  padding-bottom: 70px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n.slide.slide--first {\n  background-image: url(\"/assets/img/background2.jpg\");\n  background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover;\n  height: 100vh;\n  justify-content: space-between;\n}\n.slide:nth-child(odd) {\n  background-color: #132d74;\n  color: white;\n}\n.slide:nth-child(odd) b,\n.slide:nth-child(odd) ul li::before {\n  color: white;\n}\n.slide:nth-child(odd) .box {\n  border: 2px solid white;\n}\n.footer a, .slide--first a {\n  color: white;\n  text-decoration: underline;\n}\n.footer .footer__menu, .slide--first .footer__menu {\n  position: absolute;\n  bottom: 10px;\n  left: 0px;\n  margin: 20px 0;\n  text-align: center;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9wYWdlcy90ZXN0L3Rlc3QuY29tcG9uZW50LnNjc3MiLCIvVXNlcnMvY29yY29kZS93b3JrcGxhY2UvY29yc2lrL2NvcnNpY2EtcGFzc3BvcnQtd2ViL3NyYy9hcHAvcGFnZXMvdGVzdC90ZXN0LmNvbXBvbmVudC5zY3NzIiwiL1VzZXJzL2NvcmNvZGUvd29ya3BsYWNlL2NvcnNpay9jb3JzaWNhLXBhc3Nwb3J0LXdlYi9zcmMvc2Nzcy12YXJpYWJsZXMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUNFaEI7RUFDSSx5QkNITTtFRElOLFlDRG9CO0FGQ3hCO0FDR0E7RUFDSSxtQkFBQTtFQUNBLGdCQUFBO0FEQUo7QUNJQTtFQUNJO0lBQ0ksaUJBQUE7RURETjtBQUNGO0FDSUE7RUFDSSxrQkFBQTtBREZKO0FDS0E7RUFDSSxnQkFBQTtFQUFrQiwyQkFBQTtBRER0QjtBQ0lBO0VBQ0ksWUFBQTtFQUFtQiw0REFBQTtFQUNuQixjQzdCTTtFRDZCVyxxQkFBQTtFQUNqQixpQkFBQTtFQUFtQiw4QkFBQTtFQUNuQixxQkFBQTtFQUF1Qix3REFBQTtFQUN2QixVQUFBO0VBQVksNENBQUE7RUFDWixpQkFBQTtFQUFtQiw0Q0FBQTtBREt2QjtBQ0ZBO0VBQ0ksY0NyQ007QUYwQ1Y7QUNGQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QURLSjtBQ0ZBO0VBQ0ksaUJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7QURLSjtBQ0hJO0VBQ0Msb0RBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSxhQUFBO0VBQ0EsOEJBQUE7QURLTDtBQ0ZJO0VBQ0kseUJDbkVFO0VEb0VGLFlDakVnQjtBRnFFeEI7QUNIUTs7RUFFSSxZQ3BFWTtBRnlFeEI7QUNGUTtFQUNJLHVCQUFBO0FESVo7QUNLSTtFQUNJLFlBQUE7RUFDQSwwQkFBQTtBREZSO0FDS0k7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0VBRUEsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBREpSIiwiZmlsZSI6ImFwcC9wYWdlcy90ZXN0L3Rlc3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAY2hhcnNldCBcIlVURi04XCI7XG4ucHJpbWFyeS1ibG9jayB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxMzJkNzQ7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmxvZ28ge1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBtYXgtd2lkdGg6IDQwMHB4O1xufVxuXG5AbWVkaWEgKG1heC1oZWlnaHQ6IDQwMHB4KSB7XG4gIC5jb250ZW50LWFsaWduIHtcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcbiAgfVxufVxuaDEsIGgyLCBoMywgaDQsIGg1IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG51bCB7XG4gIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIC8qIFJlbW92ZSBkZWZhdWx0IGJ1bGxldHMgKi9cbn1cblxudWwgbGk6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwi4oCiXCI7XG4gIC8qIEFkZCBjb250ZW50OiBcXDIwMjIgaXMgdGhlIENTUyBDb2RlL3VuaWNvZGUgZm9yIGEgYnVsbGV0ICovXG4gIGNvbG9yOiAjMTMyZDc0O1xuICAvKiBDaGFuZ2UgdGhlIGNvbG9yICovXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAvKiBJZiB5b3Ugd2FudCBpdCB0byBiZSBib2xkICovXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgLyogTmVlZGVkIHRvIGFkZCBzcGFjZSBiZXR3ZWVuIHRoZSBidWxsZXQgYW5kIHRoZSB0ZXh0ICovXG4gIHdpZHRoOiAxZW07XG4gIC8qIEFsc28gbmVlZGVkIGZvciBzcGFjZSAodHdlYWsgaWYgbmVlZGVkKSAqL1xuICBtYXJnaW4tbGVmdDogLTFlbTtcbiAgLyogQWxzbyBuZWVkZWQgZm9yIHNwYWNlICh0d2VhayBpZiBuZWVkZWQpICovXG59XG5cbmIge1xuICBjb2xvcjogIzEzMmQ3NDtcbn1cblxuLmJveCB7XG4gIGJvcmRlcjogMnB4IHNvbGlkICMxMzJkNzQ7XG4gIG1hcmdpbjogMTBweDtcbiAgcGFkZGluZzogMTBweCAyMHB4O1xuICBtaW4taGVpZ2h0OiAxMjBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLnNsaWRlIHtcbiAgbWluLWhlaWdodDogNzAwcHg7XG4gIHBhZGRpbmctdG9wOiA3MHB4O1xuICBwYWRkaW5nLWJvdHRvbTogNzBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4uc2xpZGUuc2xpZGUtLWZpcnN0IHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWcvYmFja2dyb3VuZDIuanBnXCIpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGhlaWdodDogMTAwdmg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5zbGlkZTpudGgtY2hpbGQob2RkKSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxMzJkNzQ7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5zbGlkZTpudGgtY2hpbGQob2RkKSBiLFxuLnNsaWRlOm50aC1jaGlsZChvZGQpIHVsIGxpOjpiZWZvcmUge1xuICBjb2xvcjogd2hpdGU7XG59XG4uc2xpZGU6bnRoLWNoaWxkKG9kZCkgLmJveCB7XG4gIGJvcmRlcjogMnB4IHNvbGlkIHdoaXRlO1xufVxuXG4uZm9vdGVyIGEsIC5zbGlkZS0tZmlyc3QgYSB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59XG4uZm9vdGVyIC5mb290ZXJfX21lbnUsIC5zbGlkZS0tZmlyc3QgLmZvb3Rlcl9fbWVudSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAxMHB4O1xuICBsZWZ0OiAwcHg7XG4gIG1hcmdpbjogMjBweCAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xufSIsIkBpbXBvcnQgJy4vLi4vLi4vLi4vc2Nzcy12YXJpYWJsZXMuc2Nzcyc7XG5cbi5wcmltYXJ5LWJsb2NrIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeTtcbiAgICBjb2xvcjogJGZvbnQtY29sb3Itb24tcHJpbWFyeTtcbn1cblxuLmxvZ28ge1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgbWF4LXdpZHRoOiA0MDBweDtcbiAgICAvLyBtYXJnaW4tYm90dG9tOiAxMDBweDtcbn1cblxuQG1lZGlhIChtYXgtaGVpZ2h0OiA0MDBweCkge1xuICAgIC5jb250ZW50LWFsaWduIHtcbiAgICAgICAgcGFkZGluZy10b3A6IDIwcHg7XG4gICAgfVxufVxuXG5oMSwgaDIsIGgzLCBoNCwgaDUge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxudWwge1xuICAgIGxpc3Qtc3R5bGU6IG5vbmU7IC8qIFJlbW92ZSBkZWZhdWx0IGJ1bGxldHMgKi9cbiAgfVxuICBcbnVsIGxpOjpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXFwyMDIyXCI7ICAvKiBBZGQgY29udGVudDogXFwyMDIyIGlzIHRoZSBDU1MgQ29kZS91bmljb2RlIGZvciBhIGJ1bGxldCAqL1xuICAgIGNvbG9yOiAkcHJpbWFyeTsgLyogQ2hhbmdlIHRoZSBjb2xvciAqL1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkOyAvKiBJZiB5b3Ugd2FudCBpdCB0byBiZSBib2xkICovXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrOyAvKiBOZWVkZWQgdG8gYWRkIHNwYWNlIGJldHdlZW4gdGhlIGJ1bGxldCBhbmQgdGhlIHRleHQgKi9cbiAgICB3aWR0aDogMWVtOyAvKiBBbHNvIG5lZWRlZCBmb3Igc3BhY2UgKHR3ZWFrIGlmIG5lZWRlZCkgKi9cbiAgICBtYXJnaW4tbGVmdDogLTFlbTsgLyogQWxzbyBuZWVkZWQgZm9yIHNwYWNlICh0d2VhayBpZiBuZWVkZWQpICovXG59XG5cbmIge1xuICAgIGNvbG9yOiAkcHJpbWFyeTtcbn1cblxuLmJveCB7XG4gICAgYm9yZGVyOiAycHggc29saWQgJHByaW1hcnk7XG4gICAgbWFyZ2luOiAxMHB4O1xuICAgIHBhZGRpbmc6IDEwcHggMjBweDtcbiAgICBtaW4taGVpZ2h0OiAxMjBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5zbGlkZSB7XG4gICAgbWluLWhlaWdodDogNzAwcHg7XG4gICAgcGFkZGluZy10b3A6IDcwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDcwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgJi5zbGlkZS0tZmlyc3Qge1xuICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9hc3NldHMvaW1nL2JhY2tncm91bmQyLmpwZycpO1xuICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgIGhlaWdodDogMTAwdmg7XG4gICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICB9XG5cbiAgICAmOm50aC1jaGlsZChvZGQpIHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnk7XG4gICAgICAgIGNvbG9yOiAkZm9udC1jb2xvci1vbi1wcmltYXJ5O1xuICAgICAgICBiLFxuICAgICAgICB1bCBsaTo6YmVmb3JlIHtcbiAgICAgICAgICAgIGNvbG9yOiAkZm9udC1jb2xvci1vbi1wcmltYXJ5XG4gICAgICAgIH1cblxuICAgICAgICAuYm94IHtcbiAgICAgICAgICAgIGJvcmRlcjogMnB4IHNvbGlkIHdoaXRlO1xuICAgICAgICB9XG4gICAgfVxufVxuXG4uZm9vdGVyLCAuc2xpZGUtLWZpcnN0IHtcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjksIDI5LCAyOSk7XG4gICAgLy8gY29sb3I6IHdoaXRlO1xuICAgIC8vIHBhZGRpbmc6IDIwcHg7XG4gICAgYSB7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG4gICAgfVxuXG4gICAgLmZvb3Rlcl9fbWVudSB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgYm90dG9tOiAxMHB4O1xuICAgICAgICBsZWZ0OiAwcHg7XG4gICAgICAgIC8vIGJvcmRlci10b3A6IDFweCBzb2xpZCB3aGl0ZTtcbiAgICAgICAgbWFyZ2luOiAyMHB4IDA7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuICAgIC8vIHVsIGxpOjpiZWZvcmUge1xuICAgIC8vICAgICBjb2xvcjogd2hpdGU7XG4gICAgLy8gfVxufSIsIiRwcmltYXJ5OiAjMTMyZDc0OyAvLyAjNGM5ZmQ2O1xuJHByaW1hcnkyOiAjZTJmM2ZmO1xuJHNlY29uZGFyeTogcmdiKDEyMywgMTM5LCAyMDkpO1xuJGZvbnQtY29sb3Itb24tcHJpbWFyeTogd2hpdGU7Il19 */");

/***/ }),

/***/ "./src/app/pages/test/test.component.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/test/test.component.ts ***!
  \**********************************************/
/*! exports provided: TestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestComponent", function() { return TestComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TestComponent = class TestComponent {
    constructor() { }
    ngOnInit() {
    }
};
TestComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-test',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./test.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/test/test.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./test.component.scss */ "./src/app/pages/test/test.component.scss")).default]
    })
], TestComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/corcode/workplace/corsik/corsica-passport-web/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map